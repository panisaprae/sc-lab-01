package Model;

public class AutoShopMachine {
	private int price ;
	
	public void BuyFood(MyCard card, int price){
		card.setAmount(card.getAmount() - price);
	}
	
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
}
