package Model;

public class MyCard {
	private int amount;
	
	public MyCard(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}
	
	public void setAmount(int amount){
		this.amount = amount;
	}
}
