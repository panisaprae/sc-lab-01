package Model;


public class AutoMachine {
	private int balance;

	public void AddValue(MyCard card,int value){
		card.setAmount(card.getAmount() + value);
	}
	
	public int getBalance(){
		return balance;
	} 
	
	public void setBalance(int balance) {
		this.balance = balance;
	}
}
