package Viewer;

import java.util.Scanner;

import Controller.Controller;


public class Viewer {
	private Controller controller;
	
	public Viewer(Controller controller) {
		this.controller = controller;
	}

	public void showUserCommand() {
		System.out.println("");
		System.out.println("Please choose command");
		System.out.println("1. Refill money card.");
		System.out.println("2. Buy food.");
		System.out.println("3. Show balance money in money card.");
		System.out.println("4. Exit.");
		System.out.println("---------------------------");
		System.out.print("Enter your command : ");
		
	}
	
	public void getUserInput()  {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		if (n == 1) {
			System.out.println("Use command : 1. Refill money card.");
			System.out.print("Enter money to refill. : ");
			int value = sc.nextInt();
			controller.setAddValue(value);

		} else if (n == 2) {
			System.out.println("Use command : 2. Buy food.");
			System.out.print("Enter  cost. : ");
			int cost = sc.nextInt();
			controller.setBuyFood(cost);
			System.out.println("Buy food success.");
			
		} else if (n == 3) {
			System.out.println("Use command : 3. Show remaining money in money card.");
			System.out.println("You have " + controller.getAmount() + " bath in money card.");
			
		} else if (n == 4) {
			System.out.println("Use command :  Exit"); 
			System.exit(0); 
		}
	}
}
