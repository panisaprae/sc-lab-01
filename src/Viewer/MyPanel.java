package Viewer;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
//import java.awt.JPanel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controller.Controller;
import Model.AutoMachine;
import Model.AutoShopMachine;
import Model.MyCard;

public class MyPanel extends JFrame{
	int balance=0;
	MyCard card;
	AutoShopMachine shopmachine;
	AutoMachine machine;
	Controller controller;
	
	public MyPanel (String titel) {
		// TODO Auto-generated method stub		
		super ( titel );
		 card = new MyCard(0);
		 shopmachine = new AutoShopMachine();
		 machine = new AutoMachine();
		 controller = new Controller(card, machine, shopmachine);
		
		
		JPanel p = new JPanel();
		JPanel pp = new JPanel();
		JPanel ppp = new JPanel();
		
		p.setBackground(Color.orange);
		p.setSize(250, 150);
		pp.setBackground(Color.pink);
		pp.setSize(250, 150);
		ppp.setBackground(Color.yellow);
		ppp.setSize(500,300);
		setBackground(Color.black);
		p.setLocation(0,0);
		pp.setLocation(250, 0);
		ppp.setLocation(0, 150);
		setLayout(null);
		setSize(500, 300);;
		
		
		
		final Label labelAutomachine = new Label("AutoMachine");
		
		//Label label2 = new Label("Your Money");
		p.add(labelAutomachine);
		//p.add(label2);
		
		final TextField tf = new TextField("" , 25);
		p.add(tf);
		
		JButton b = new JButton ("add your money");
				b.setForeground(Color.black);
		b.setBackground(Color.white);
		b.setSize(20, 5);
		p.add(b);
		
		
		
		
		
		
		
		Label labelShopMachine = new Label("Shop");
		pp.add(labelShopMachine);
		
		final TextField tf2 = new TextField("" , 25);
		pp.add(tf2);
		
		Button b2 = new Button ("buying");
		b2.setForeground(Color.black);
		b2.setBackground(Color.white);
		b2.setSize(20, 5);
		pp.add(b2);
		
		
		
		final JLabel labelBalance = new JLabel("Your Money  : ");
		ppp.add(labelBalance);
		add(p);
		add(pp);
		add(ppp);
		b.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				 //balance  += Integer.parseInt(tf.getText());
				 controller.setAddValue(Integer.parseInt(tf.getText()));
				 String s = "Your Money  : " + controller.getAmount();
				 labelBalance.setText(s);
			}
		
		}); 
		b2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				 //balance  -= Integer.parseInt(tf2.getText());
				 controller.setBuyFood(Integer.parseInt(tf2.getText()));
				 String s = "Your Money  : " + controller.getAmount();
				 labelBalance.setText(s);
			}
		
		}); 

		setVisible(true);
		setResizable(false);
		
		
	}
	
	public static void main (String args[])
	{
		
		MyPanel f = new MyPanel("GUI Absolute Layout");
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);

	}
}
