package Viewer;

import Controller.Controller;
import Model.MyCard;
import Model.AutoMachine;
import Model.AutoShopMachine;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyCard card = new MyCard(0);
		AutoShopMachine shopmachine = new AutoShopMachine();
		AutoMachine machine = new AutoMachine();
		Controller controller = new Controller(card, machine, shopmachine);
		Viewer view = new Viewer(controller);
		while (true){
			view.showUserCommand();
			view.getUserInput();
		}	
	} 
}

