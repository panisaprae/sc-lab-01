package Controller;
import Model.MyCard;
import Model.AutoMachine;
import Model.AutoShopMachine;


public class Controller {

	private MyCard card;
	private AutoMachine machine;
	private AutoShopMachine shopmachine;
	
	public Controller(MyCard card, AutoMachine machine, AutoShopMachine shopmachine){
		this.card = card;
		this.machine = machine;
		this.shopmachine = shopmachine;
	}

	public void setAmount(int amount) {
		card.setAmount(amount);
	}
	
	public int getAmount(){
		return card.getAmount();
	}

	public void setAddValue(int value){
		machine.AddValue(card, value);
	}
	
	public int getBalance(){
		return machine.getBalance();
	}
	
	public void setBuyFood(int price){
		shopmachine.BuyFood(card, price);
	}
	
	public int getPrice(){
		return shopmachine.getPrice();
	}
	
}
